========================
INF5153 H14 UQAM
========================

3 TP : 3 itérations, choix d'une méthodologie agile, 

Voir le plan de cours & les diapositives, 

Examen intra : 5 mars 2014
Examen final : 23 avril 2014

À livre fermer
 
tp1 : 20 fev.
tp2 : 20 mars.
tp3 : 24 avril.

Design Patterns: Elements of reusable object oriented software 1994-5-7

--------------------------------------------------

**Definition** Génie Logiciel : Ensemble d'activité qui permet de développer des logiciel pour supporter les processus d'affaire des entreprises. 

Ex : Processus d'affaire pour faire des soumission, echanger des infos, generer des contrats etc, et ce processus necessite des logiciels pour les supporter. 

Voir **IEEE** , Notation des processus d'affaires : BPMN. C'est le standard omg qui permet de decrire un processus d'affaire. 

Processus d'affaire / métier ==> système d'information......étapes connus / communes. 

Model Drive Architecture.

Modele independant d'une plateforme. modele de conception generique. 


Definition conception
~~~~~~~~~~~~~~~~~~~~~

À quelle question : How ? C'est le comment , l'analyse répond à la question quoi . 
Conception Orienté Object c'est comment mais sous forme d'objet + leur interaction. 

Artefact
~~~~~~~~

Modèle representant la conception sous diverses formes , 

Livrable
~~~~~~~~

Sont le plus souvents des documents.
Ça peut être une matrice et d'autre type de documents

pour la conception OOB, les livrables sont : Document de conception architecturale et document de conception détaillée

correspond au TP. 

On peux verifier que notre conception est bonne ? 

--------------------------------

**Outil qui permettent de generer du code**

On évalue avec des **métrique**.

Metrique disponibles : 

  1. Cohesion / Couplage , c'est l'independance fonctionnelle, si el rapport ~ infinie --> conception == bonne ! 
       On doit maximiser la **cohesion** et diminuer le **couplage** 
  2. Le degré de liens logiques entre les methodes d'une classe. 
  3. Le couplage : niveau d'interaction entre deux ou plusieurs composants logiciels. 

Associations vs Héritage pour éviter l'arborescence de mongole ...

Patron d'architecture & Patron de conception
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Patron conception : solution conceptuelle approuvé , à un probleme de conception. 

Patron d'architecture : n-niveau, ex : application distribué , 
  * Singleton patron de conception qui permet de creer une seule instance d'un objet. 

autre exemple de conception 
   

classe A ----> classe Bimpl
          / \
          | |
      Couplage right there. 

classe A : 
   Bimpl b;
   b = new Bimpl;

-----------------------------------------


===========================================



Cours 2 : Processus de développement
------------------------------------

Sites sources : theserverside.com & infoq.com

Software engineering process. 

qui quoi quant comment

visions => developpement logiciel ==> logiciel.

Années 60-70 surcout et retards, massifs !
Cascade spirale prototypage methodes agile


Modele cascade
~~~~~~~~~~~~~~

Approche disciplinée mais tr;es rigide, succes ds petit projet, mais echec pour les gros project. 


Prototypage
~~~~~~~~~~~

Faire un prototype, 2 type de prototype : 

- Jetable
- Evolutif

Voir **pressman**

Inspiré du modele en spiral


Spirale
~~~~~~~

Analyse du risque, 


UP Processus Unifié
~~~~~~~~~~~~~~~~~~~

Viens de UML. 
Itérative & incrémentale
ce sera traité en cours. 
Exige beaucoup de documentation. 

Méthode agile
~~~~~~~~~~~~~

Fevrier 2001, gerer par Agile Alliance
Promuvoir les principes / methodes agiles. 
Produire un logiciel testé plutot que de la documentation claire.
Exigent de faire participer le client. 
Notion de user stories - non formel.
Use case == modele d'analyse 
Satisfaire le client en livrant tot et regulierement 
accepter les changement, meme tardivement dans le developpement, 
Livrable fréquent d'une application fonctionnel.
Iteration timebox , on peux pas couper ou allonger la durée d'une itération
batir le projet autour de personnes motivés
la methode la plus efficace est le user story
La conception fait partie de la methodologie agile mais elle doit etre simplifié.

Artefact & livrable 
~~~~~~~~~~~~~~~~~~~

On créer un artefact, puis des livrable en conception

Diagramme de collaboration
~~~~~~~~~~~~~~~~~~~~~~~~~~

Plus pratique d'utiliser un diagramme de séquence.


Produit operation plutot que la documentation exhaustice 
collaboration avec le client au lieu de negociation d'un contrat 
et l'adaptation au changements plutot que le suivi d'un plan. 

Courtes itération + feedback + vite + precie sur + petite tache + facile s'adapter au surprises. 


----------------------------------------

Documentation très courte 
Modeliser les parties difficile ou delicates
Niveau de modelisation minimalement suffisant
Outils simple et adaptés aux groupes. 

Garder sa simple mais si qqchose de toutché, garder ces artefacts. 

Scrum , XP 

AUP / RUP

Lean Software Development.

**Scrum** c'est comme mieux la...

----------------------------------------

Product owner produit le carnet du produit, sous forme d'un back log , qui propose des use-case.

**Transparence** 
**Inspection**
**Adaptation**

Individu :
~~~~~~~~~~

Scrum Master
Équipe de dev
Project Owner

le srum master n'est pas un chef de projet.

Un sprint est une iteration
les sprint sont limité dans le temps (time box)

Use case, analyse, 

2 part reunion : quoi ? 
                 comment ? 

15 minutes, daily scrum

3 question 

quesser ta fait hier
quesser tu fait aujourdhiui
as tu des problemes ? 

Voir certification scrum. ~1500$ 2-3 jour workshop + ajout valeur a mcgill.
pyxis -> laval createur d'agile mtl-qc..


Extreme Programming
~~~~~~~~~~~~~~~~~~~

Travail en binome, et test driven dev.

Revue de codes continue, 
test unitaire + test fonctionnel constament, 
refactoring constant
conception simple
itération très curtes.

valeur xp

communication
simpliciter
feedback 

pratiques 

incrementale
programmation a deux
conception simple
test unitaires enpremier TDD.

le refactoring 
~~~~~~~~~~~~~~

Benefique mais risqué car danger de briser ce qui fonctionne déjà.
Dangeureux si pas de test unitaires complets. 

L'intégration présente des défis architectural. 

Integration continue
~~~~~~~~~~~~~~~~~~~~

Le code est public appartient à tout le monde
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Le processus Unifié
--------------------

Grady Booch, OO

Jacobson OOSE

James Rumbaugh, OMT.

OMG n'as pas été capable de normalisé OMT, 

RUP synthese de bcq de bonnes pratiques de software dev.
Generique, orienté use-case, centrer sur l'architecture

architecture systeme des les premiers incréments. 

Comment on fait ca ?

On s'attaque au use-case qui couvre l'architecture. 
Les increments doivent etre court , 

Iteration 

modele : 

business modelling reuirement analysis , implementation, test et deployablemnt

business modelling 

c'est l'analyse des processus d'affaires. 

--------------------------------------

Environnements 
~~~~~~~~~~~~~~

Developpement ------

test ------ QA. avec des données camouflés, 

pre-production (Staging) ------- Machines équivalente à la production, les données réelles, les donnés clients. Sert pour les tests d'acceptations.

Production --------  

Activité de bases
Activité secondaire

----------------------------------

Point de depart du developpement logiciel.

Document de vision , idnetifier les acteurs ( utilisateurs et autres systèmes)

USE-CASE 
performance, fiabilité, reutilisabilité, etc. , exigences non fonctionnelles.

Modele de classes
use case diagrame 
premier diagram d'architecture 


Faire des tests a partir des uses-cases. QA doit dériver des tests cases des uses-cases. 

La modelisation formelle est utiliser dans les applications qui sont critiques, OCL.

Business Modelling
~~~~~~~~~~~~~~~~~~

fin cours 2
~~~~~~~~~~~

============================================================

Cours 3
-------

TP1
~~~~

Jeu Connect Fou (Puissance 4)

Conception architecturale : Couplage & Cohésion

Methodologie Agile : Scrum / XP 
Si application web, alors open source et gratuit et specifier le protocole et le conteneur dans remise. 
l'imoortant est de faire une bonne conception. 

Use-case diagram.
Diagramme de package.
Diagramme de classe, 
diagramme de sequence.
diagramme de deploiement.
un prototype graphique.

remise en fichier compresser .zip, pour le 20 février.

-------------------------------------

L'industrie logiciel et UML 
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Le nombre d'artefact decoule du fait qu'il y ai plusieurs acteurs.
Les plus importants : Diagramme de classe, diagramme comportementale, de collaboration, de sequence, au minimum. 
UML: Viens de Booch, OMT et OOSE, OMT est l'ancêtre d'UML.

Par exemple UML: Est normalisé, par contre le processus unifié n'est pas encore normalisé. 

14 Diagrammes : Structurels, Comportementaux et Comportementaux **spécifiques**

1. de classe / d'objet
2. de paquetage
3. de composants et de déploiement

4. de cas d'utilisation
5. d'états-transitions (automate à états finis)

6. de séquences
7. de communication (collaboration)

----------------------------------

Concept de base
~~~~~~~~~~~~~~~

Diagrame de cas d'utilisation : acteur , systeme, cas d'utilisation
include, extend, generalisation

include c'est un use case qui inclus un autre use case
extend 

**QUESTIONS** : S'Authentifier dans un include != V == cas d'Utilisation ? 
Ça dépends de l'acteur est-ce que c'est une action importante pour l'acteur ? 

**Architecture de la meta modelisation**

 * creer des modele qui sont UML valide. (Synthaxiquement), Le modèle est une **instance** d'UML.
   Une classe Livre est une instance d'une classe UML. Une instance de cette classe est un objet : instance du monde reel.

Cadre EMF (Eclipse Modeling Framework)

Methode abstraite == classe abstraite == non instanciable. 

Verification par modele, l'instance doit etre conforme au modèle. 

Validation == repond au meta modele et aussi au contraintes.Penses au language OCL, validation USE attributs.

L'état d'une classe est l'ensemble de ses attributs, sans attribut , une classe est dite : stateless.

**diagrame d'objet ~ ocl invariants, pour valider et expliquer le diagramme de classe.**

Entre objet on etablie des liens, dans le diagramme d'objet : genre une ligne - fleche entre case 

nomObjet : Type
: TypeClasseAnon

Un lien a une direction : et un nom bavard : verbe a l'infinitif.

Entre classe on parle d'Association
Entre objets on parle de Liens. 
Voir p.41 diapositive. 
Notions de **Cardinalité**.

Super classe, sous classe, generalisation, specialisation. 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Architecture des meta modelisation

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Agrégation : variante has a de l'association, notion d'appartenance
agrégation forte == composition. 

l'agregation et la composition sont des notions flexible dependants du contexte. 

Diagramme de séquence. communication , choisir entre les deux. 
DIagramme de communication est le diagramme privilegier dans les processus agile. 

Diagramme d'état-transition est un automate à l'état fini. Transition et conditions, evenement et condition genre variable = valeur, cest tjs une expression Booleenne. 


~~~~~~~~~~~~~~~~~~~~~~~

Generalisation / Specialisation / Association : UML .

----------------------------

Conception architecturale 
~~~~~~~~~~~~~~~~~~~~~~~~~

Couche logiciel, couche physique aussi, 

Exercices 3

class FigGeo
class FigComp & class FigSimple

Voir Patron de conception **Composite**. 
Par exemple la structure de repertoire, un repertoire peut contenir des fichiers ou des repertoires. 

~~~~~~~~~~~~~~~~~~~~~

Conception Architecturale

Introduction : L'architecture et les critere de qualités, concevoir une architecture avec UML. En final les differents style architecturaux. 

Qu'est-ce que l'architecture logiciel ? : definit le logiciel en terme de composantes et d'interactions entre composantes
Architecture inclus les composants, proprieter ext visible des composants
et les relations entre les composants

c'est quoi un composant ? : Les systemes externes en boites noires par exemple. il peut s'agir d'un système. il se peut que l'architecture employé influence 

votre systeme comporte des composants qui interagissent entre eux, et votre sys peut se connecter a un systeme externe pour aller chercher des donnée par exemple. il faut montrer les deux systeme , car cest une architecture, ne pas oublier les contraites, genre le protocoles , le comment est indispensable. 

Il se peut qu'om ne puisse pas communiquer sans modifier le systeme legataire , au niveau de la notations O->
represente un service par interface. SOA , 

C'est la decomposition d'un systeme en composants et les interactions de ces composants. 
C'est apres l'analyse et avant la conception détaillé.

Elle doit repondre au use-case et aux critere non fonctionnel. Souvent on fait prevaloir les critere par rapport a daute, genre on sen fou si cest pas portable, ou le choix specific de microsoft etc...contraite non fonctionnels car tu saute des etape et elimine une certaine complexité, cest le but.

»Developper les systeme dinformation pour supporter les processus d'affaire, le manque 

Notions de couche, 
Point d'extension pour montrer que larchitecture repond a une certaine charge mais sans rien changé, permet faire une mise à l'échelle. 

observable, non observable et business quality

observable ^= performance / securité / disponibilité / fonctionnalité (use-case) / utilisabilité

non observable = modifiabilité , integrabilité : on parle des interfaces.., testabilité.

Conception architecturale avec UML
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

3 diagrammes :
  
Vue architecturales selon un point de vue architecturale, norme iso 19501 2005.

Artefact relatif : SAD = IEEE 1471-2000 

vue logique = diagramme de package
vue dimplementation = diagramme de composant
deploiement = diagramme de deploiement et de package. 
vue de processus = diagramme activité / etat-transition . 

import / access ~ explicité le comportement par defaut, bon pour les lectures futures ~ car les valeurs par defaut peuvent changer dans les versions des frameworks utilisé .

difference entre classe et composant : la distinction est vague. un ca exige qu'on utilise une interface, et le composant doit etre autonome et remplaçable,. 

UN COMPOsante doit minimalement implementer une interface, 

Interface_Service <----- Service_IMP <== composante.

un client ne peut pas creer un lien avec l'implementation, il est lier a l'interface et cest tout 
normalement un composant ne peut pas etre instancier sans passer par linterface, des fois elle est privé et on doit passer par une "home interface" qui va pouvoir nous donner acces a ce composant via une interface. 

pour aller chercher le home en general, il est accessible via un naming service. 
pour chaque home omn a un identificateur, URL_1 2 3 ... etc..

on doit tjs passer par son interface, au niveau du code sa donne qqchose comme 


Client qui utilise interface_service, ..., instancier avec "interface_service = new service_implementation" tout le code va etre basé sur l'interface, soit l'interface home ou l'injecteur donnera le code, pas de service_implementation simpl = .... est malsain , pas bon . . 


Diagramme de deploiement
~~~~~~~~~~~~~~~~~~~~~~~~

Noeud physique & Noeud d'environnement d'execution 

~~~~~~~~~~~~~~~~~~~~~~~~~~~

On forme des couches dans un diagramme de paquetage qui contienne des classes,
les classes forment des composant pour le diagramme de composant, et on deploie les composants dans des noeuds de deploiement (diagramme de deploiement).

Style architecturale
~~~~~~~~~~~~~~~~~~~~~~

separation des soucis, (separation of concerns)
dissimulation de l'information 

supporter le developpement en parrallèle , 
on doit isoler les specificite de la plateforme, ou de produits commerciaux. 

Simple et uniforme et les modules doivent etre separer entre celle qui produise (fournissent un service) et celle qui recoivent un service (qui consomme) . 
le choix d'UN ARChitecture depends des exigeances fonctionnelle et non fonctionnelle du logiciel, 
Influencé par des styles architecturaux. un style architectural c'est un patron , une solution recurrente ;a un probleme d'»architecture. 

voir p.13 style architecturaux 

pipeline, sgbd, mvc, multi couche, n couche, iriente services (SOA), ROA, ESB. . . 

Pipeline
~~~~~~~~

Filtre et Canal, 

Referenciel de donnée
~~~~~~~~~~~~~

SGBD , actif et passif , de plus en plus en noSql. ~= sans sgbd, pas necessairement non relationelle. 
cest bon pour les données nombreuse et changeant souvent mais mauvais pour les deadlock 
t1 = a,b lock sur a et t2 = b,a lock sur b, cest un dead lock . 

------------------------------------------------------------

Patron de conception **GRASP** et **GOF**.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour l'examen livre et 3 page recto verso permise.

Retour sur pattern d'architecture

separation of concerns, dissimu;ation de l'information, 
doit supporter dev en parallele, separer les modules produire des donn.ées de ceux qui les consommeent. 

MVC : Modele vue controlleur 

modele : regle d'affaire 
vue : presentation à l'utilisateur
controleur : interaction entre vue et modele. 

controlleur : patron de conception 
~~~~~~~~~~~~~~~

Front controller, common pattern (GOF)

Tout le monde passe par un seul controlleur (1 seul implementer sous forme de singleton :: une seule instance).
Eviter les bottleneck, le controlleur doit etre synchrone, sans bloquer les requetes. 

SOA
~~~~~

Architecture Orienté Service (service oriented architecture). C'est un style architectural modulaire , distribué`, faiblement couplé.

Les services font partie de la couche modele. les services communiquent entre eux via un bus de donnée, (entreprise service bus).

SOA vs N-niveau. 

Detaillé
~~~~~~~~

Responsabilité, collaboration, voir gabarit CRC.

Voir grasp dans larman : bonne pratiques plus que des patron de conceptions. 
Voir loi de Demeter : clean code. 

--------------------


Revision sur TP / Examen
~~~~~~~~~~~~~~~~~~~~~~~

Diagramme de Séquence, 

Instance A1 va appeller une methode de Instance A2. et doit etre une methode de A2.
Classe en majuscule, methode en minuscule.

Continuer patron gof
~~~~~~~~~~~~~~~~~~~~~

Creation
Structure
Comportement

Composite, Visiteur, Observateur. Lors de la derniere scéance.

Singleton : 

Motivation : une seule instance de la classe. 
             Constructeur privé pour garantir une seule instance. 
             créer une méthode static getInstance(); 
             ce patron définit une opération de classe getInstance() qui retourne un instance unique.

             l'accès est controller et peut être adapté pour un nombre variables d'instances. 
             à partir de la méthode , modifier pour implémenter l'idée d'un pool. 

             public class Singleton {
              
             private static Singleton instance = null;
             private Singleton(){}
             public final synchronized static Singleton getINstance(){
               if(instance != null) instance = new Singleton();
               return instance;
              }

             }

            etc..
           exemple en java
          

           Le patron Singleton est relié aux patrons suivants : Prototype & Fabrique Abstraite & Monteur.

Le patron controlleur est un singleton. 

Le patron abstract factory
-------------------------

Une application 2 systeme qui utilise deux sorte de fenetre differente. 
Famille de produit, à une seule fabrique, guarantir le bon produit selon l'environnement injecté. 
cinq produit == cinq methode de creation. 

pour chaque type d'environnement on va creer une sousclasse. 
le seul default est que si on ajoute une nouvelle famille de produit il faut changer chaque sous classe, 
pas comme visiteur, on doit modifier la classe abstraite et chacune des sous classes. 

comparativement a visiteur : on a tout simplement a ajouter une sous classe, cest facile. 
on veut que le client utilise une seule interface, 

Le patron Commande
-------------------

Le patron mémento
------------------

Le patron mémento
------------------

Ces 3 patrons peuvent être utile pour le tp2 !

TP2 est rendu au 6 avril. et la demo sera le 2 avril. 

Le patron Adapter
-----------------

Pouvoir adapter une autre classe pour notre interface, on ajoute notre classe adapter 

Le patron decorateur
--------------------

Composant auquel on ajoute des décoration (composant separer des decorateur, creer des objets decorateur pour chaque decoration qu'on veut ajouter, quand
on appelle la decoration on appel la meme methode de la composante et on lui ajoute des fonctionnalité 

EXAMEN : Iterateur : Base de donnée, iterateur qui permet de modifier agregat, un deuxieme iterateur va avoir du trouble , 
strong iterator, est une implementation , gerer ce type de parcours sans copier la liste, 

Patron facade : 
~~~~~~~~~~~~~~~

interface frontale qui sert des sous-systeme (1..*).

Patron Iterateur
~~~~~~~~~~~~~~~
 
fin

---------------------------------------

Patron fabrique, pont, ...

Patron Pont (handle, body) patron gof. 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Découpler une abstraction de ses implémentations.
pour chaque abstraction on doit avoir plusieurs implementations. 

voir le livre pdf. 

Patron proxy
~~~~~~~~~~~~~

fournit une classe qui substitut pour une autre classe. 

----------------------------------

Decorator
~~~~~~~~~

une famille de produit == une famille de factory sous forme de singleton, fonctionne toujours
rassembler tout les objets et creer une seule classe qui creer ses produits, garder une semantique, genre db et dao &!
pas creer une seule fabrique, une fabrique pour les db et une fabrique pour les dao, 

jamaais plus d'une seule instance de fabrique , alors fabrique == singleton, separer en couche, minimalement, trois couche. 

certain nombre d'objets et services etc.. 


Antpatron
~~~~~~~~~~
 
UNe mauvaise solution à un probleme, qui doit etre reparer par du refactoring !
Des fois facile a detecter d'autre fois non, 
bcq de sujet de recherche ds ce domaine, detecté mauvaise solutions, etc. 
appliquer un patron cest facile car les structures sont bien définie. qu'on peut detecté. , plusieurs facon de detecter si un patron existe dans 
un programme, etc...

Blob (god class)
code spaghetti

faire ressortir les methodes qui parlent de la meme chose dans le blob et creer une nouvelle classe (ou trouver une classe candidate). 

les mauvais signes : 

code dupliqué, longue methode, large classe, longue liste de parametres. 
divergent change : une calsse est modifier de diff maniere pour diff raison ca vaut la peine de diviser la classe de sorte que chaque partie soit associeter a un type de changement particulier. 

shotgun surgery

un petit changement necessite qu'on change bcq de code. 

feature envy : lorsqu'une classe s'interesse plus aux atributs d'une autre classe que les siens, changer la methode de classe au lieu d'utiliser une autre classe pour jouer sur les attribut d'un autre classe. 

primitive obsession. 

hierarchie parallele d'heritage, 

les classes paresseuses : Classes qui ne font rien ou presque rien. 

generalité speculatives: 
champ temporaire: 

chaine de message entre objets

middle man qui ne fait rien , l'indirection c'est bon mais watch it 

Intimité inappropriée, methode d'un objet qui va get des attributs d'un autre objets. 

classes alternatives avec des interfaces différentes. 

modifier pour que les deux classes 

heritage refusé. si une sous classe d'utilise pas ou tres peu les methode de sa super classe

commentaires
si les commentaires sont present car le code est mauvais , alors ameliorer le code. 

Les metriques COO
~~~~~~~~~~~~~~~~~~

comment mesurer sa conception ? 

traditionnel et orienté objets

trad : 
complexité cyclomatique, taille, pourcentage de commentaire, etc...

exemple chidamber et kemerer 
~~~~~~~~~~~~~~

ligne de code, complexité cyclomatique
cohesion
couplage profondeur d'heritage, 
etc. 


voir page 28 pour concordance avec mauvaises odeurs. 
coupling between object le plus bas possible. 

depth of inheritance tree, 
le moins profond possible car l'héritage est un couplage très fort. 

metric mood, 
refactoring, 

--------------------------

Architecture d'entreprise. 
~~~~~~~~~~~~~

voir **TOGAF**.
premiere version en 1995, version actuelle 9.1, 
une methode générique , ensemble de techniques pour la transformation de l'architecture actuelle. 

Methode specifique à togaf pour batir une structure d'entreprise. 

Il se peut que l'entreprise n'ai pas de structure d'entreprise initialement , et qu'elle veuille en batir une, l'utilisation de BOGAF peut etre benefique et recommader dans ce contexte la. meme si cest le bordel au debut. 

iterative & incrementale, 

quatre type d'architecture  

affaires, 
données, 
applicatives
techniques. 

processus d'affaire. 
togaf c'est des outils, good practices, models et methodologies. 
voir **TOGAF** : proposition d'un language de modélisation. 

C'est une méthodologie souple qui peut s'adapter selon l'entreprise. 


EXAMEN 
~~~~~~~

examen patron de conceptions a partir des patrons gof + style architecturaux + architecture d'entreprise. 
