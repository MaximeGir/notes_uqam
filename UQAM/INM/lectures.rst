============================
LECTURES OBLIGATOIRES, NOTES
============================

PFIZER
------

*****************************************************************

Le taux de succes est faible 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Empressement a mettre en place le changement par les gestionnaires. 
2. Perte de vue des objectifs fondamentaux du changement.
3. Firme externe = plus compliquer qu'un système sur-mesure pour l'entreprise.

Les Équipes
~~~~~~~~~~~

A. Les "parrains" - équipe qui légitimise , approuve le projet. 
B. Commité de direction, (steering comitee), font rapport au parrains, ils entretiennent la vision. 
C. Groupe de projet qui avait les deux mains dedans, un chef de projet , accompagné par un consultant en gestion de changement. qui s'intégre à l'équipe. 

Mise en place d'une équipe solide avec des roles bien définis , qui sont là pour ça, qui font rien d'autre.
Ils orchestrent le changement, l'ajuste, l'adapte. Comme la conduite automobile. 

Le lancement officiel
~~~~~~~~~~~~~~~~~~~~~

Une grosse soirée avec tout les employés touchés, les membres de la directions, les parrains, le comité de direction et les consultants, une présentation du projet pour que tout le monde sente l'importance du changement à venir et l'importance de mener à bien ce projet. (tlm doit sentir la vision!).

La Communication
~~~~~~~~~~~~~~~~

A. Communication **régulière** et à toute les étapes du projets, entre tout les éléments d'une équipe et entre toute les équipes (ce qui inclus les utilisateurs).

B. S'adapter aux particularités des différents groupes concernés. 

::

    Démarche basé sur la compréhension des impacts et des enjeux humains

C. S'assurer que tout le monde ait reçu une formation qui leur permettent de s'adapter et d'utiliser les nouveaux outils numérique implanté

Voir Tableau. 

============================================================================================

Changement techno : Gagnon
--------------------------

Problèmes :
    1. 75% des projets informatiques échouent 
    2. L'implantation d'un nouveau système apporte des nouveau problèmes et ne règles pas ceux pour lesquels on a décidé d'implanter un nouveau système informatique !!

Ces échecs se mesurent par le fait que 

1. Le projet est **arrêté en cours de route**, 
2. Le système **ne répond pas aux exigences** opérationnelles de l’organisation,
3. Le coût du projet **dépasse** de beaucoup **le budget prévu** (on parle souvent du double), 
4. **L’échéancier** d’implantation n’est pas respecté 
5. Les utilisateurs sont insatisfaits 

A. Ce n'est pas la technologie qui améliore la performance. 
B. C'est la façon dont les humains l'utilisent qui améliore les performances. 

::

    Les utilisateurs doivent **s'approprier** le changement technologique. 
    Pour se l'approprier, ils doivent le **comprendre.**

C. On doit tenir compte de la capacité de changer l'organisation plus que l'aspect technique. 


**Voir diagrame de la flamme olympique**

Mais en gros : 
 
 1. L'état de la connaissance + la logique d'action des intervenants 
  - dynamique entre les intervenant 
   - Se base sur le contexte spécifique de l'organisation et de son histoire. 

 2. Le projet est né avec ses propriété. 
  - Échéancier
  - Budget
  - Technologie
 
 3. La réussite du projet passe par
  - L'Atteinte des objectifs opérationnel.

1. Urgence du projet
2. Implication, le plus tôt possible, d’un large nombre d’employés touchés
3. Compréhension de la complexité et des impacts du projet par la haute direction
4. Prévision des effets de turbulence dans l’organisation
5. Communication et coopération interfonctionnelles  - L'Acceptation de la technologie. 

Avis expert 
~~~~~~~~~~~

1. Orienter les principes et le contenu vers la personne plutôt que vers la technologie 
2. S’adresser à chaque groupe d’intervenants, partie prenante du changement technologique 
3. Promouvoir un questionnement et faire cheminer les personnes quant au changement qu’ils ont à vivre
4. Être concret, précis et simple à utiliser pour chaque intervenant 
5. Viser la flexibilité par rapport au milieu, au type de technologie et au style de gestion, afin qu’il puisse être utilisé dans différents contextes 
6. Favoriser le développement d’une culture organisationnelle d’adaptation au changement

Truc
~~~~

Système de carnets qui définit les parametre pour un certain groupe d'usager (d'acteur). 
On peut consulter celui des autres pis les autres peuvenmt consulter le notre aussi. 
ca rapproche la dynamique des intervenant , cest la liaison, au fond. 

Le carnet contient des fiches , ---> l'état des connaissances. qui laisse la latitude necessaire au membre d'appliquer leur logique d'action tout en respectant le travail des autres *leur progression*

=================================================================================

Equipe Confiance
----------------

Notions : 

La confiance est **une décisions réfléchie** et non un état d'esprit
 - **On choisit** à qui ont fait confiance, 
 - **On choisit** QUAND on fait confiance, 
 - **Avoir confiance représente un risque**, 

La confiance est une **perte de controle** en laissant à l'autre certaines initiatives. 
Cela comporte des risques et la personne ayant accordé sa confiance en patit **en premier**.

1 Créer un environnement protecteur
2 Établir des liens de proximité avec son interlocuteur
3 Faire la preuve de sa performance
4 Se montrer prévisible
5 Adopter une communication proactive 

Trois truc 
~~~~~~~~~~

A. Vérifier sa tolérance au risque 
B. Confiance en soi , s'assurer que notre difficulté a faire confiance n'émane pas d'un manque de confiance en soi, 
C. Perception de control, vérifier les sénarios déclencher par une trahison de sa confiance et ainsi déterminer si le jeu en vaut la chandelle. 

======================================================================================

Equipe dispersé / rapproché 
---------------------------

Problématique du travail à distance. 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  - Les conflits ne peuvent se résoudre aussi facilement et les moment de tension ne peuvent être détendu aussi rapidement que dans la vrai vie, genre les non-dit, le ton emprunter dans les courriel, qqun qui pense que lautre est bête....etc..

  - Vu que l'équipe ne se voit jamais , chacun des element va se sentir detacher physiquement de psychologiquement de l'équipe et va se sentir plus pres des gens geographiquement pres de lui/elle, meme si ce nest pas des gens qui font partie intégrante de sa propre équipe...

  - Augmentation de stress
   - vu que tu travail a la maison, ta famille te sollicite plus , genre appelle le plombier, va chercher le petit a la garderie etc...
   - D'autre se surinvestissent et exagere , subisse la pression de ne pas se sentir adéquat, par la peur qu'ils ont de passer pour des lâches qui se la coule douce. 

Le manager
~~~~~~~~~~
































=================================================================================================

RDPRM
-----

Résumé de lecture 
~~~~~~~~~~~~~~~~

Notion de partenariats : Définition dans le dictionnaire 
 - Association d'entreprises , d'institutions en vue de mener une action commune. 

Spécification RDPRM : Registre des droits personnels et réels mobiliers. 

Les deux partie concernant le partenariat 

  1. Le gouvernemnt du Québec. **PUBLIC**
    - concevoir et assurer la prestation de services publics à une collectivité. 
    - Efficacité , équité, imputabilité, developpement économique

  2. LGS. **PRIVÉ**
    - Générer de la valeur pour leurs actionnaires , bénéfices commerciaux. 
   
Les natures contradictoires des deux organisations ( l'une veux générer des bénéfices sociaux, l'autre des bénéfices commerciaux) n'est généralement pas considérer de façon adéquate dans l'établissement d'un partenariat public-privé.

=============================================================================================================

Facteurs de réussite d'un tel partenariat
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Coopération actives des acteurs 
* Dynamique relationnelle d'ajustements mutuels et de recherche d'avantages communs
* Une certaines pérennité de la relation. 

**PPP : C'est une coopération vers l'atteinte de bénéfices sociaux et commerciaux en partageant les risques et les revenus équitablement.Pour une durée de temps limités ou illimité.**

 









 





